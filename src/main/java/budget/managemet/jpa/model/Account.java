package budget.managemet.jpa.model;

import budget.managemet.config.Restriction;
import budget.managemet.exceptions.NotFoundElement;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@ToString(exclude = {"fixedExpensesList","budgetList"})
@EqualsAndHashCode(exclude = {"fixedExpensesList","budgetList"})
@Getter
@Setter
@Entity
@Table(name = "Account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    @NotNull
    @Setter(AccessLevel.PROTECTED)
    @Column(name = "id")
    private long id;

    @NotNull
    @Size(min = Restriction.MIN_LENGTH, max = Restriction.MAX_LENGTH)
    @Column(name = "login" , unique = true)
    private String login;

    @JsonIgnore
    @NotNull
    @Column(name = "password")
    private String password;

    @NotNull
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "permission")
    private Permission permission;

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    @Setter(AccessLevel.PROTECTED)
    @OneToMany(mappedBy = "account")
    private List<FixedExpenses> fixedExpensesList = new ArrayList<>();

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    @Setter(AccessLevel.PROTECTED)
    @OneToMany(mappedBy = "account")
    private List<Budget> budgetList = new ArrayList<>();

    public void addFixedExpenses(FixedExpenses fixedExpenses){
        fixedExpensesList.add(fixedExpenses);
    }

    public void addBudget(Budget budget){
        budgetList.add(budget);
    }

    public final static Account createAccount(String login, String password, Permission permission){
        Account account = new Account();
        account.setLogin(login);
        account.setPassword(password);
        account.setPermission(permission);
        return account;
    }

}
