package budget.managemet.jpa.model;

import budget.managemet.config.Restriction;
import budget.managemet.enumeration.TypeOfExpense;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "Fixed_Expenses")
public class FixedExpenses {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Setter(AccessLevel.PROTECTED)
    @Column(name = "id")
    private long id;

    @NotNull
    @Size(min = Restriction.MIN_LENGTH, max = Restriction.MAX_LENGTH)
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "value", columnDefinition="Decimal(15,2) default '0.00'", scale = 2)
    private BigDecimal value;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeOfExpense type;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "account")
    private Account account;

    public static final FixedExpenses createFixedExpense(String name, BigDecimal value, TypeOfExpense typeOfExpense, Account account){
        FixedExpenses fixedExpenses = new FixedExpenses();
        fixedExpenses.setName(name);
        fixedExpenses.setValue(value);
        fixedExpenses.setType(typeOfExpense);
        fixedExpenses.setAccount(account);
        return fixedExpenses;
    }

}

