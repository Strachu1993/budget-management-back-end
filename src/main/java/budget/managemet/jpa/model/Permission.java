package budget.managemet.jpa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "Permission")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    @NotNull
    @Setter(AccessLevel.PROTECTED)
    @Column(name = "id")
    private long id;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "name" , unique = true)
    private String name;

}




