package budget.managemet.jpa.model;

import budget.managemet.config.Restriction;
import budget.managemet.enumeration.TypeOfExpense;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "Budget")
public class Budget {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Setter(AccessLevel.PROTECTED)
    @Column(name = "id")
    private long id;

    @Column(name = "date")
    private LocalDate date;

    @NotNull
    @Size(min = Restriction.MIN_LENGTH, max = Restriction.MAX_LENGTH)
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "value", columnDefinition="Decimal(15,2) default '0.00'", scale = 2)
    private BigDecimal value;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeOfExpense type;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "account")
    private Account account;

    public static final Budget createBudget(LocalDate date, String name, BigDecimal value, TypeOfExpense type, Account account){
        Budget budget = new Budget();
        budget.setDate(date);
        budget.setName(name);
        budget.setValue(value);
        budget.setType(type);
        budget.setAccount(account);
        return budget;
    }

}
