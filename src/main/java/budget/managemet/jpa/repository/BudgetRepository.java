package budget.managemet.jpa.repository;

import budget.managemet.jpa.model.Account;
import budget.managemet.jpa.model.Budget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface BudgetRepository extends JpaRepository<Budget, Long> {

    List<Budget> findByAccount(Account account);

    List<Budget> findByAccountAndDate(Account account, LocalDate date);

    Budget findByAccountAndId(Account account, long id);

}
