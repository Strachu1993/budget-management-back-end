package budget.managemet.jpa.repository;

import budget.managemet.jpa.model.Account;
import budget.managemet.jpa.model.FixedExpenses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FixedExpensesRepository extends JpaRepository<FixedExpenses, Long> {

    List<FixedExpenses> findByAccount(Account account);

    FixedExpenses findByAccountAndId(Account account, long id);

}
