package budget.managemet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan(value="budget.managemet")
public class BudgetManagementServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(BudgetManagementServerApplication.class, args);
	}
}
