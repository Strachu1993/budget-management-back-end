package budget.managemet;

import budget.managemet.enumeration.TypeOfExpense;
import budget.managemet.jpa.model.Account;
import budget.managemet.jpa.model.Budget;
import budget.managemet.jpa.repository.AccountRepository;
import budget.managemet.jpa.repository.BudgetRepository;
import budget.managemet.jpa.repository.FixedExpensesRepository;
import budget.managemet.jpa.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;

@Component
public class OnStrart {

    @Autowired
    private PermissionRepository p;

    @Autowired
    private AccountRepository a;

    @Autowired
    private FixedExpensesRepository f;

    @Autowired
    private BudgetRepository b;

    @PostConstruct
    public void onStrat(){
        System.out.println("\n\n");

        p.findAll().forEach(System.out::println);

        System.out.println("\n");

        a.findAll().forEach(System.out::println);

        System.out.println("\n");

        f.findAll().forEach(System.out::println);

        System.out.println("\n");

        datePrepare(1);
        datePrepare(2);

        System.out.println("\n\n");
    }

    private void datePrepare(long userId) {
        LocalDate date = LocalDate.of(2017, 05 - (int)userId, 1);
        Account account = a.findOne(userId);

        for(int i=2 ; i<7 ; i++)
            b.save(Budget.createBudget(date.minusMonths(i), "name"+i, new BigDecimal("50.00"), TypeOfExpense.MINUS, account));

        for(int i=1 ; i<3 ; i++) {
            b.save(Budget.createBudget(date.minusMonths(i), "name" + i, new BigDecimal("10.00"), TypeOfExpense.PLUS, account));
            b.save(Budget.createBudget(date.minusMonths(i), "name" + i, new BigDecimal("10.00"), TypeOfExpense.PLUS, account));
        }

        b.findAll().forEach(System.out::println);
    }

}
