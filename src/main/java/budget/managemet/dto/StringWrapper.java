package budget.managemet.dto;

import lombok.Data;

@Data
public class StringWrapper {

    private String text;

    public StringWrapper(String text) {
        this.text = text;
    }
}
