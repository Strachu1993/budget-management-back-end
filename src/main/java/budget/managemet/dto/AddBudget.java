package budget.managemet.dto;

import budget.managemet.enumeration.TypeOfExpense;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class AddBudget {

    private String name;
    private BigDecimal value;
    private TypeOfExpense type;
}
