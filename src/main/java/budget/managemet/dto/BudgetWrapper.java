package budget.managemet.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.math.BigDecimal;
import java.time.LocalDate;

@AllArgsConstructor
@Data
public class BudgetWrapper {

    private LocalDate date;
    private BigDecimal minusSum;
    private BigDecimal plusSum;
    private BigDecimal sum;

}
