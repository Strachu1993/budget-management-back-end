package budget.managemet.config.webfilters;

import budget.managemet.dto.JwtUser;
import budget.managemet.services.JwtService;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/user/*"})
public class JwtUserFilter implements Filter {

    @Autowired
    private JwtService jwtTokenService;

    @Value("${jwt.auth.header}")
    private String authHeader;

    @Override
    public void init(FilterConfig filterConfig) {}

    @Override public void destroy() {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final HttpServletResponse httpResponse = (HttpServletResponse) response;

        if (filterToken(httpRequest, httpResponse))
            return;

        chain.doFilter(httpRequest, httpResponse);
    }

    private boolean filterToken(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        if("OPTIONS".equalsIgnoreCase(httpRequest.getMethod())) {
            httpResponse.setStatus(HttpServletResponse.SC_OK);
        }else{
            final String authHeaderVal = httpRequest.getHeader(authHeader);

            if (checkTokenIsNotNull(httpResponse, authHeaderVal))
                return true;

            try {
                checkToken(authHeaderVal);
            } catch(JwtException e) {
                httpResponse.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                return true;
            }
        }
        return false;
    }

    private boolean checkTokenIsNotNull(HttpServletResponse httpResponse, String authHeaderVal) {
        if (null == authHeaderVal) {
            httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return true;
        }
        return false;
    }

    private void checkToken(String authHeaderVal) {
        JwtUser jwtUser = jwtTokenService.getUser(authHeaderVal);
        if(!"ROLE_USER".equalsIgnoreCase(jwtUser.getRole()))
            throw new JwtException("Wrong rules");
    }

}