package budget.managemet.config;

public enum Restriction {;

    public static final int MIN_LENGTH = 1;
    public static final int MAX_LENGTH = 60;
    public static final int LENGTH = 60;

    public static final int minLengthPass = 8;

    public static final String regexBudgetData = "[0-9]{4}-[0-9]{2}";
    public static final String regexExpensesName = "[a-zA-ZĄąŚśĆćŹźÓóŁłĘęŃń]{1}[a-zA-Z0-9- ĄąŚśĆćŹźÓóŁłĘęŃń]*";
}
