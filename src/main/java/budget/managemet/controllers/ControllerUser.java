package budget.managemet.controllers;

import budget.managemet.dto.AddBudget;
import budget.managemet.jpa.model.Budget;
import budget.managemet.jpa.model.FixedExpenses;
import budget.managemet.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor(onConstructor = @__({@Autowired}))
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class ControllerUser {

    private final UserService userService;
    //private final MessageSource messageSource;

    @PutMapping(value = "/changePassword")
    public HttpEntity changePassword(@RequestHeader("${jwt.auth.header}") String token, @RequestBody Map<String, Object> data){
        return userService.changePassword(token, data);
    }

    @GetMapping(value = "/getFixedExpenses")
    public List<FixedExpenses> getFixedExpenses(@RequestHeader("${jwt.auth.header}") String token){
        return userService.getFixedExpenses(token);
    }

    @PutMapping(value = "/changeFixedExpenses")
    public HttpEntity changeFixedExpenses(@RequestHeader("${jwt.auth.header}") String token, @RequestBody FixedExpenses fixedExpenses){
        return userService.changeFixedExpenses(token, fixedExpenses);
    }

    @DeleteMapping(value = "/deleteFixedExpenses")
    public HttpEntity deleteFixedExpenses(@RequestHeader("${jwt.auth.header}") String token, @RequestParam Long id){
        return userService.deleteFixedExpenses(token, id);
    }

    @PostMapping(value = "/addFixedExpenses")
    public HttpEntity addFixedExpenses(@RequestHeader("${jwt.auth.header}") String token, @RequestBody FixedExpenses newFixedExpenses){
        return userService.addFixedExpenses(token, newFixedExpenses);
    }

    @GetMapping(value = "/getBudget")
    public HttpEntity getBudget(@RequestHeader("${jwt.auth.header}") String token){
        return userService.getBudget(token);
    }

    @PostMapping(value = "/addBudget")
    public void addBudget(@RequestHeader("${jwt.auth.header}") String token, @RequestBody List<AddBudget> budgets, @RequestParam String date){ // @DateTimeFormat(pattern = "MM/yyyy") LocalDate
        userService.addBudget(token, budgets, date);
    }

    @GetMapping(value = "/getBudgetByDate")
    public HttpEntity getBudgetByDate(@RequestHeader("${jwt.auth.header}") String token, @RequestParam String date){
        return userService.getBudgetByDate(token, date);
    }

    @DeleteMapping(value = "/deleteBudgetById")
    public HttpEntity deleteBudgetById(@RequestHeader("${jwt.auth.header}") String token, @RequestParam long id){
        return userService.deleteBudgetById(token, id);
    }

    @PutMapping(value = "/changeBudget")
    public HttpEntity changeBudget(@RequestHeader("${jwt.auth.header}") String token, @RequestBody Budget budget){
        return userService.changeBudget(token, budget);
    }

}
