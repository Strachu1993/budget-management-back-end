package budget.managemet.controllers;

import budget.managemet.services.AnonymousService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor(onConstructor = @__({@Autowired}))
@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class ControllerAnonymous {

    @Autowired
    private AnonymousService anonymousService;

    @PostMapping(value = "/login")
    public HttpEntity auth(@RequestBody Map<String, Object> data) { //ResponseEntity<?>
        return anonymousService.loginModule(data);
    }

    @PostMapping(value = "/registration")
    public HttpEntity userRegistration(@RequestBody Map<String, Object> data){
        return anonymousService.registrationModule(data);
    }

}
