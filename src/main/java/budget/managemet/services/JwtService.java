package budget.managemet.services;

import budget.managemet.dto.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import javax.annotation.PostConstruct;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class JwtService {

    @Value("${jwt.expire.hours}")
    private Long expireHours;

    @Value("${jwt.token.secret}")
    private String plainSecret;

    private String encodedSecret;

    @PostConstruct
    protected void init() {
        this.encodedSecret = generateEncodedSecret(this.plainSecret);
    }

    protected String generateEncodedSecret(String plainSecret) {
        if (StringUtils.isEmpty(plainSecret))
            throw new IllegalArgumentException("JWT secret cannot be null or empty.");
        return Base64.getEncoder().encodeToString(this.plainSecret.getBytes());
    }

    protected Date getExpirationTime() {
        return new Date(TimeUnit.HOURS.toMillis(expireHours) + new Date().getTime());
    }

    protected JwtUser getUser(String encodedSecret, String token) { // podczas wywoływania metody
        Claims claims = Jwts.parser().setSigningKey(encodedSecret).parseClaimsJws(token).getBody();

        JwtUser securityUser = new JwtUser();
        securityUser.setLogin(claims.getSubject());
        securityUser.setRole((String) claims.get("role"));
        return securityUser;
    }

    public JwtUser getUser(String token) {
        return getUser(this.encodedSecret, token);
    }

    protected String getToken(String encodedSecret, JwtUser jwtUser) { // podczas logowania
        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(jwtUser.getLogin())
                .claim("role", jwtUser.getRole())
                .setIssuedAt(new Date())
                .setExpiration(getExpirationTime())
                .signWith(SignatureAlgorithm.HS512, encodedSecret)
                .compact();
    }

    public String getToken(JwtUser jwtUser) {
        return getToken(this.encodedSecret, jwtUser);
    }

}