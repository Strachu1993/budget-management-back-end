package budget.managemet.services;

import budget.managemet.dto.StringWrapper;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Locale;

@Service
public class BasicOperations {

    private MessageSource messageSource;

    public boolean isNotNull(Object object){
        return null != object;
    }

    public StringWrapper getMessageFromProperties(String source){
        return new StringWrapper(messageSource.getMessage(source, null, Locale.getDefault()));
    }

    public LocalDate getMontAndYearByText(String date){
        int s = date.indexOf('-');
        int month = Integer.parseInt(date.substring(0, s));
        int year = Integer.parseInt(date.substring(s+1));
        return LocalDate.of(year, month, 1);
    }

}
