package budget.managemet.services;

import budget.managemet.dto.JwtUser;
import budget.managemet.dto.StringWrapper;
import budget.managemet.exceptions.BadPasswordsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class AnonymousService {

    @Autowired
    private JpaService jpaService;

    @Autowired
    private JwtService jwtService;

    public HttpEntity loginModule(Map<String, Object> data) {
        String login = data.get("login").toString();
        String password = data.get("password").toString();

        if (jpaService.authorization(login, password) ) {
            JwtUser jwtUser = new JwtUser(login, jpaService.getPermissionByLogin(login));
            return ResponseEntity.ok(new StringWrapper(jwtService.getToken(jwtUser)));
        }

        return new ResponseEntity<>(new StringWrapper("Błąd logowania"), HttpStatus.UNAUTHORIZED);
    }

    public HttpEntity registrationModule(Map<String, Object> data) {
        String login = data.get("login").toString();
        String password = data.get("password").toString();
        String passwordAgain = data.get("passwordAgain").toString();

        if(!jpaService.registration(login, password, passwordAgain))
            throw new BadPasswordsException();

        return ResponseEntity.ok(new StringWrapper("Stworzono użytkownika"));
    }

}
