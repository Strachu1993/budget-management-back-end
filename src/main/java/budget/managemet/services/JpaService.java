package budget.managemet.services;

import budget.managemet.config.Restriction;
import budget.managemet.dto.AddBudget;
import budget.managemet.dto.BudgetWrapper;
import budget.managemet.enumeration.TypeOfExpense;
import budget.managemet.jpa.model.Account;
import budget.managemet.jpa.model.Budget;
import budget.managemet.jpa.model.FixedExpenses;
import budget.managemet.jpa.repository.AccountRepository;
import budget.managemet.jpa.repository.BudgetRepository;
import budget.managemet.jpa.repository.FixedExpensesRepository;
import budget.managemet.jpa.repository.PermissionRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class JpaService {

    private AccountRepository accountRepository;
    private PermissionRepository permissionRepository;
    private FixedExpensesRepository fixedExpensesRepository;
    private BudgetRepository budgetRepository;
    private BasicOperations basicOperations;
    private PasswordEncoder passwordEncoder;

    public boolean authorization(String login, String password){

        Account account = accountRepository.findByLogin(login);

        if(basicOperations.isNotNull(account) && basicOperations.isNotNull(password)){
            if(passwordEncoder.matches(password, account.getPassword())){
                return true;
            }
        }
        return false;
    }

    public String getPermissionByLogin(String login){
        return accountRepository.findByLogin(login).getPermission().getName();
    }

    public boolean registration(String login, String password, String passwordAgain){
        if(password.equals(passwordAgain)) {
            if(password.length() >= Restriction.minLengthPass){
                accountRepository.save(Account.createAccount(login, passwordEncoder.encode(password), permissionRepository.findByName("ROLE_USER")));
                return true;
            }
        }
        return false;
    }

    /////////////////////////////////---------> USER <----------\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    public boolean changePassword(String login, String oldPassword, String newPassword, String repeatPassword){
        Account account = accountRepository.findByLogin(login);

        if(basicOperations.isNotNull(account) && newPassword.equals(repeatPassword) && newPassword.length() >= Restriction.minLengthPass && passwordEncoder.matches(oldPassword, account.getPassword())){
            account.setPassword(passwordEncoder.encode(newPassword));
            accountRepository.save(account);
            return true;
        }
        return false;
    }

    public List<FixedExpenses> getFixedExpenses(String login){
        return fixedExpensesRepository.findByAccount(accountRepository.findByLogin(login));
    }

    public FixedExpenses changeFixedExpenses(String login, FixedExpenses newFixedExpenses){
        Account account = accountRepository.findByLogin(login);
        FixedExpenses fixedExpenses;

        if(basicOperations.isNotNull(account)){
            fixedExpenses = fixedExpensesRepository.findByAccountAndId(account, newFixedExpenses.getId());
            if(basicOperations.isNotNull(fixedExpenses)){
                fixedExpenses.setName(newFixedExpenses.getName());
                fixedExpenses.setValue(newFixedExpenses.getValue());
                fixedExpenses.setType(newFixedExpenses.getType());
                fixedExpensesRepository.save(fixedExpenses);
            }
        }
        return fixedExpensesRepository.findByAccountAndId(account, newFixedExpenses.getId());
    }

    public void deleteFixedExpenses(String login, long id){
        Account account = accountRepository.findByLogin(login);
        fixedExpensesRepository.delete(fixedExpensesRepository.findByAccountAndId(account, id));
    }

    public FixedExpenses addFixedExpenses(String login, FixedExpenses newFixedExpenses){
        Account account = accountRepository.findByLogin(login);

        if(null != account){
            FixedExpenses fixedExpenses = FixedExpenses.createFixedExpense(newFixedExpenses.getName(), newFixedExpenses.getValue(), newFixedExpenses.getType(), account);
            fixedExpensesRepository.save(fixedExpenses);
            return fixedExpenses;
        }

        return null;
    }

    public List<BudgetWrapper> getBudget(String login){
        List<Budget> budgets = budgetRepository.findByAccount(accountRepository.findByLogin(login));
        List<BudgetWrapper> result = new ArrayList<>();

        List<LocalDate> distinctDate = budgets
                .stream()
                .map(Budget::getDate)
                .distinct()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

        BigDecimal minusSum;
        BigDecimal plusSum;

        for(LocalDate d : distinctDate){
            minusSum = getSum(budgets, d, TypeOfExpense.MINUS);
            plusSum = getSum(budgets, d, TypeOfExpense.PLUS);
            result.add(new BudgetWrapper(d, minusSum, plusSum, plusSum.subtract(minusSum)));
        }

        return result;
    }

    private BigDecimal getSum(List<Budget> budgets, LocalDate d, TypeOfExpense type) {
        return budgets
                .stream()
                .filter(b -> b.getDate().equals(d))
                .filter(b -> b.getType().equals(type))
                .map(Budget::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Transactional
    public int addBudget(String login, LocalDate date, List<AddBudget> budgets){
        Account account = accountRepository.findByLogin(login);
        int count = 0;

        if(null != account)
            for(AddBudget b : budgets) {
                budgetRepository.save(Budget.createBudget(date, b.getName(), b.getValue(), b.getType(), account));
                count++;
            }
        else
            throw new NullPointerException();

        return count;
    }

    public List<Budget> getBudgetByDate(String login, LocalDate date){
        return budgetRepository.findByAccountAndDate(accountRepository.findByLogin(login), date);
    }

    public void deleteBudgetById(String login, long id){
        Account account = accountRepository.findByLogin(login);
        budgetRepository.delete(budgetRepository.findByAccountAndId(account, id));
    }

    public Budget changeBudget(String login, Budget oldBudget){
        Account account = accountRepository.findByLogin(login);
        Budget budget;

        if(basicOperations.isNotNull(account)){
            budget = budgetRepository.findByAccountAndId(account, oldBudget.getId());
            if(basicOperations.isNotNull(budget)){
                budget.setName(oldBudget.getName());
                budget.setValue(oldBudget.getValue());
                budget.setType(oldBudget.getType());
                budgetRepository.save(budget);
            }
        }

        return budgetRepository.findByAccountAndId(account, oldBudget.getId());
    }

}