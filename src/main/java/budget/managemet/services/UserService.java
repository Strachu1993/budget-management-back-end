package budget.managemet.services;

import budget.managemet.dto.AddBudget;
import budget.managemet.dto.StringWrapper;
import budget.managemet.exceptions.BadPasswordsException;
import budget.managemet.exceptions.NotFoundElement;
import budget.managemet.jpa.model.Budget;
import budget.managemet.jpa.model.FixedExpenses;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class UserService {

    private final JpaService jpaService;
    private final JwtService jwtService;
    private final BasicOperations basic;

    public HttpEntity changePassword(String token, Map<String, Object> data) {
        String oldPassword = data.get("oldPassword").toString();
        String newPassword = data.get("newPassword").toString();
        String repeatPassword = data.get("repeatPassword").toString();

        if(!jpaService.changePassword(jwtService.getUser(token).getLogin(), oldPassword, newPassword, repeatPassword)){
            throw new BadPasswordsException();
        }

        return ResponseEntity.ok(new StringWrapper("Hasło zostało zmienione"));
    }

    public List<FixedExpenses> getFixedExpenses(String token){
        return jpaService.getFixedExpenses(jwtService.getUser(token).getLogin());
    }

    public HttpEntity changeFixedExpenses(String token, FixedExpenses newFixedExpenses){
        String login = jwtService.getUser(token).getLogin();
        FixedExpenses fixedExpenses = jpaService.changeFixedExpenses(login, newFixedExpenses);

        if(null == fixedExpenses)
            throw new NotFoundElement();

        return ResponseEntity.ok(fixedExpenses);
    }

    public HttpEntity deleteFixedExpenses(String token, long id){
        jpaService.deleteFixedExpenses(jwtService.getUser(token).getLogin(), id);
        return ResponseEntity.ok(new StringWrapper("Usunięto"));
    }

    public HttpEntity addFixedExpenses(String token, FixedExpenses newFixedExpenses){
        String login = jwtService.getUser(token).getLogin();
        FixedExpenses fixedExpenses = jpaService.addFixedExpenses(login, newFixedExpenses);

        if(null == fixedExpenses)
            throw new NotFoundElement("Brak konta");

        return ResponseEntity.ok(fixedExpenses);
    }

    public HttpEntity getBudget(String token){
        return ResponseEntity.ok(jpaService.getBudget(jwtService.getUser(token).getLogin()));
    }

    public HttpEntity addBudget(String token, List<AddBudget> budgets, String date){
        String login = jwtService.getUser(token).getLogin();
        LocalDate d = basic.getMontAndYearByText(date);

        return ResponseEntity.ok("Liczba dodanych rekordów: " + jpaService.addBudget(login, d, budgets));
    }

    public HttpEntity getBudgetByDate(String token, String date){
        String login = jwtService.getUser(token).getLogin();
        return ResponseEntity.ok(jpaService.getBudgetByDate(login, basic.getMontAndYearByText(date)));
    }

    public HttpEntity deleteBudgetById(String token, long id){
        String login = jwtService.getUser(token).getLogin();
        jpaService.deleteBudgetById(login, id);
        return ResponseEntity.ok(new StringWrapper("Usunięto"));
    }

    public HttpEntity changeBudget(String token, Budget budget){
        String login = jwtService.getUser(token).getLogin();
        Budget newBudget = jpaService.changeBudget(login, budget);

        return ResponseEntity.ok(newBudget);
    }

}
