package budget.managemet.exceptions;

import budget.managemet.config.Restriction;
import budget.managemet.dto.StringWrapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ExceptionsHandlers extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ NullPointerException.class, ClassCastException.class, StringIndexOutOfBoundsException.class })
    public ResponseEntity<Object> handleNullPointerExceptionException(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new StringWrapper("Złe parametry"), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({ ConstraintViolationException.class, TransactionSystemException.class })
    public ResponseEntity<Object> handleConstraintViolationExceptionException(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex, new StringWrapper("Złe dane"), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({ DataIntegrityViolationException.class })
    public ResponseEntity<Object> handleDataIntegrityViolationException(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex, new StringWrapper("Już istnieje"), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({ BadPasswordsException.class })
    public ResponseEntity<Object> hadnleBadPasswordsException(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex, new StringWrapper("Hasła nie są jednakowe lub sa za krótkie powinno być " + Restriction.minLengthPass + " znaków"), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({ NotFoundElement.class, InvalidDataAccessApiUsageException.class })
    public ResponseEntity<Object> hadnleNotFoundElementToChangeException(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex, new StringWrapper("Brak elementu " + ex.getMessage()), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }


}
