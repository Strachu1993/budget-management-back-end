package budget.managemet.exceptions;

public class NotFoundElement extends RuntimeException {

    public NotFoundElement() {
        super();
    }

    public NotFoundElement(String message) {
        super(message);
    }

}
