INSERT INTO Permission VALUES (1, 'ROLE_USER');
-- INSERT INTO Permission VALUES (2, 'ROLE_ADMIN');

-- $2a$10$JCI7d2LgiEy16fBGqkJMwup/CSz9dKRcZF.ytCxvLVPwHhK1.welK -> admin

INSERT INTO Account (id, login, password, permission) VALUES (1, 'siemak', '$2a$10$JCI7d2LgiEy16fBGqkJMwup/CSz9dKRcZF.ytCxvLVPwHhK1.welK', 1);
INSERT INTO Account (id, login, password, permission) VALUES (2, 'names', '$2a$10$JCI7d2LgiEy16fBGqkJMwup/CSz9dKRcZF.ytCxvLVPwHhK1.welK', 1);
-- INSERT INTO Account (id, login, password, permission) VALUES (3, 'eloelo', '$2a$10$JCI7d2LgiEy16fBGqkJMwup/CSz9dKRcZF.ytCxvLVPwHhK1.welK', 1);

INSERT INTO Fixed_Expenses (id, name, value, type, account) VALUES (1, 'prąd', 0, 'PLUS', 1);
INSERT INTO Fixed_Expenses (id, name, value, type, account) VALUES (2, 'Gaz', 1200.11111, 'PLUS', 1);
INSERT INTO Fixed_Expenses (id, name, value, type, account) VALUES (3, 'Czynsz', 1500, 'PLUS', 1);
INSERT INTO Fixed_Expenses (id, name, value, type, account) VALUES (4, 'Telefon', 4.44444, 'MINUS', 1);

-- INSERT INTO Budget (id, date, account) VALUES (1, 'Jan-11-1999', 1); --'2017-06-22'